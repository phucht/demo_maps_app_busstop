import React, { Component } from "react";
import { Platform, Text, View, StyleSheet } from "react-native";
import { Constants, Location, Permissions, MapView } from "expo";
import Store from "./Store";
import CustomMaker from "./CustomMaker";

const customStyle = [
  {
    elementType: "geometry",
    stylers: [
      {
        color: "#ebe3cd"
      }
    ]
  },
  {
    elementType: "labels",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#523735"
      }
    ]
  },
  {
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#f5f1e6"
      }
    ]
  },
  {
    featureType: "administrative",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#c9b2a6"
      }
    ]
  },
  {
    featureType: "administrative.land_parcel",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "administrative.land_parcel",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#dcd2be"
      }
    ]
  },
  {
    featureType: "administrative.land_parcel",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#ae9e90"
      }
    ]
  },
  {
    featureType: "administrative.neighborhood",
    stylers: [
      {
        visibility: "off"
      }
    ]
  },
  {
    featureType: "landscape.natural",
    elementType: "geometry",
    stylers: [
      {
        color: "#dfd2ae"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "geometry",
    stylers: [
      {
        color: "#dfd2ae"
      }
    ]
  },
  {
    featureType: "poi",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#93817c"
      }
    ]
  },
  {
    featureType: "poi.park",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#a5b076"
      }
    ]
  },
  {
    featureType: "poi.park",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#447530"
      }
    ]
  },
  {
    featureType: "road",
    elementType: "geometry",
    stylers: [
      {
        color: "#f5f1e6"
      }
    ]
  },
  {
    featureType: "road.arterial",
    elementType: "geometry",
    stylers: [
      {
        color: "#fdfcf8"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "geometry",
    stylers: [
      {
        color: "#f8c967"
      }
    ]
  },
  {
    featureType: "road.highway",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#e9bc62"
      }
    ]
  },
  {
    featureType: "road.highway.controlled_access",
    elementType: "geometry",
    stylers: [
      {
        color: "#e98d58"
      }
    ]
  },
  {
    featureType: "road.highway.controlled_access",
    elementType: "geometry.stroke",
    stylers: [
      {
        color: "#db8555"
      }
    ]
  },
  {
    featureType: "road.local",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#806b63"
      }
    ]
  },
  {
    featureType: "transit.line",
    elementType: "geometry",
    stylers: [
      {
        color: "#dfd2ae"
      }
    ]
  },
  {
    featureType: "transit.line",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#8f7d77"
      }
    ]
  },
  {
    featureType: "transit.line",
    elementType: "labels.text.stroke",
    stylers: [
      {
        color: "#ebe3cd"
      }
    ]
  },
  {
    featureType: "transit.station",
    elementType: "geometry",
    stylers: [
      {
        color: "#dfa459"
      }
    ]
  },
  {
    featureType: "transit.station.airport",
    elementType: "geometry",
    stylers: [
      {
        color: "#bcdfd2"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "geometry.fill",
    stylers: [
      {
        color: "#2f77d3"
      }
    ]
  },
  {
    featureType: "water",
    elementType: "labels.text.fill",
    stylers: [
      {
        color: "#92998d"
      }
    ]
  }
];

export default class App extends Component {
  constructor(props) {
    super(props);
    this.Store = new Store();
    let region = {
      latitude: 10.79569845884798,
      longitude: 106.6915345538491,
      latitudeDelta: 0.002947610289906422,
      longitudeDelta: 0.001690462231636047
    };
    let northeast = {
        latitude: region.latitude + region.latitudeDelta,
        longitude: region.longitude + region.longitudeDelta
      },
      southwest = {
        latitude: region.latitude - region.latitudeDelta,
        longitude: region.longitude - region.longitudeDelta
      };

    this.state = {
      location: null,
      region: {
        latitude: 10.79569845884798,
        longitude: 106.6915345538491,
        latitudeDelta: 0.002947610289906422,
        longitudeDelta: 0.001690462231636047
      },
      northeast,
      southwest,
      isChangByAuto: true,
      isChangeRegion: false,
      errorMessage: null
    };
  }

  componentWillMount() {
    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!"
      });
    } else {
      setInterval(this._getLocationAsync, 10000);
    }
  }

  _getLocationAsync = async () => {
    if (!this.state.isChangByAuto) return;
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
    }

    let location = await Location.getCurrentPositionAsync({});
    let { latitude, longitude } = location.coords;
    let { latitudeDelta, longitudeDelta } = this.state.region;
    console.log(location);
    this.setState({
      location,
      region: { latitude, longitude, latitudeDelta, longitudeDelta },
      isChangByAuto: true
    });
  };

  render() {
    let text = "Waiting..";
    if (this.state.errorMessage) {
      text = this.state.errorMessage;
    } else if (this.state.location) {
      text = JSON.stringify(this.state.location);
    }

    return (
      <MapView
        style={{ flex: 1 }}
        provider="google"
        region={this.state.region}
        showsUserLocation={true}
        followsUserLocation={true}
        customMapStyle={customStyle}
        onPress={(e)=>{
          console.log('click')
          console.log(e.nativeEvent);
        }}
        onRegionChange={region => {
          //console.log(region);
          this.setState({ region, isChangeRegion: true, isChangByAuto: false });
          this.timeout && clearTimeout(this.timeout);
        }}
        onRegionChangeComplete={region => {
          let northeast = {
              latitude: region.latitude + region.latitudeDelta,
              longitude: region.longitude + region.longitudeDelta
            },
            southwest = {
              latitude: region.latitude - region.latitudeDelta,
              longitude: region.longitude - region.longitudeDelta
            };

          this.setState({ region, northeast, southwest, isChangByAuto: true });
          this.Store.getMarker(northeast, southwest);
          let _this = this;
          this.timeout = setTimeout(function() {
            _this.setState({ isChangeRegion: false, isChangByAuto: true });
          }, 2000);
        }}
        cacheEnabled={true}
        initialRegion={this.state.region}
      >
        {this.Store.markers.map(function (marker) {
          return (
            <MapView.Marker
              key={marker._id}
              image={marker.icon}
              coordinate={{
                latitude : marker.geometry.location.lat,
                longitude : marker.geometry.location.lng,
              }}
              title={marker.name}
              description={marker.vicinity}
            >
              <CustomMaker icon={marker.icon}/>
            </MapView.Marker>
          )
        })}
        {false && (
          <MapView.Polygon
            coordinates={[
              {
                latitude: this.state.southwest.latitude,
                longitude: this.state.northeast.longitude
              },
              {
                latitude: this.state.northeast.latitude,
                longitude: this.state.northeast.longitude
              },
              {
                latitude: this.state.northeast.latitude,
                longitude: this.state.southwest.longitude
              },
              {
                latitude: this.state.southwest.latitude,
                longitude: this.state.southwest.longitude
              }
            ]}
          />
        )}
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#ecf0f1"
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: "center"
  }
});
