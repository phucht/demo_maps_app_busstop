import React, { PropTypes } from "react";
import { StyleSheet, View, Text, Image, Animated } from "react-native";
import Animation from 'lottie-react-native';

class CustomMaker extends React.Component {
  static defaultProps = {
    icon : ""
  }
  constructor(props) {
    super(props);
    this.state = {
      progress: new Animated.Value(0),
    };
  }
  componentDidMount() {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 5000,
    }).start();
  }
  render() {
    return (
    <Image source={{uri : this.props.icon}} style={{width : 50, height : 50, resizeMode: "contain"}}/>

    );
  }
}
//      <Image source={{uri : this.props.icon}} style={{width : 50, height : 50, resizeMode: "contain"}}/>
/*
*
 <Animation
 style={{
 width: 50,
 height: 50,
 }}
 loop={true}
 source={require('./TwitterHeart.json')}
 progress={this.state.progress}
 />
* */
export default CustomMaker;

